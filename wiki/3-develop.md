# Developing / Tweaking maghost

Ghost themes have a simple enough structure, that it's fairly easy to customize any theme.
This theme uses it's own logic, for better editability, fragmenting logics and avoiding duplicating code as much as possible.

Basically ghost uses a simple templating language called [Handlebars](http://handlebarsjs.com/) for its themes.

⚠️ If you get maghost from it's repository, scripts and styles are not built in assets. To test/use maghost, you'll need to build them (see the "Scripts and styles" section below for instructions on how to do it).

## The main files are at the root:

- `default.hbs` - The parent template file, which includes your global header/footer (this is the based of all following page templates)
- `home.hbs` - The main template to generate a home page
- `blog.hbs` - The main template to generate a list of posts
- `calendar.hbs` - The main template to generate the list of calendar events (similar to the `blog.hbs` page, but enhanced to display a list of events)
- `post.hbs` - The template used to render individual posts
- `page.hbs` - Used for individual pages
- `tag.hbs` - Used for tag archives, eg. "all posts tagged with `news`"
- `author.hbs` - Used for author archives, eg. "all posts written by Jamie"
- `feed.hbs` - The template used as a base for `blog.hbs`, `author.hbs`, `tag.hbs` pages
- `error-404.hbs` - Used to display a nice error page if a page wasn't found
- `error.hbs` - More radical errors page
- `meta.hbs` - Used to display the list of backgrounds, and for it to be fetched to display them on the sides of other pages

One neat trick is that you can also create custom one-off templates by adding the slug of a page to a template file. For example:

- `page-about.hbs` - Custom template for an `/about/` page
- `tag-news.hbs` - Custom template for `/tag/news/` archive
- `author-ali.hbs` - Custom template for `/author/ali/` archive

## Partials:

The `partials` directory contains code pieces reused in different page templates.

*SVG Icons:*
Maghost uses inline SVG icons, included via Handlebars partials. You can find all icons inside `/partials/icons`. To use an icon just include the name of the relevant file, e.g. To include the SVG icon in `/partials/icons/rss.hbs` - use `{{> "icons/rss"}}`.
You can add your own SVG icons in the same manner.

## Scripts and styles

All `.js` files in the `scripts` directory, and all `.less` files in the `styles` directory are aggregated to be imported in every page.
You may modify any file, or add any new one, but if you do so, you'll need to run `npm run buildDists` to reaggregate the styles and scripts.
Note that to run, the `buildDists` script (that you can edit in ./buildDists.sh) depends on the `lessc` command. You should install development dependencies for the theme running `npm install`.

The easiest thing to customize maghost is probably to customize it's colors and display. You can do so by just tweaking a few variables in the `styles/general/theme.less` file.

## Locales

The `locale` directory contains the locales used to translate the strings used in the theme into any desired language.
Contribution to translation in any new language is welcome. If you add or modify any locale, you should run `npm run buildDists` for the locales to be aggregated for import in the pages (see previous section for details).

## Assets

The assets folder contains
 - the built scripts, styles and locales imported in the pages,
 - the fonts used in the pages (imported by the theme)
 - the basic libraries used throughout the page (underscore and jquery)

## Zipping

When you're done with your modifications and want to use the theme in production, you can run `npm run zip`, that will create a `maghost.zip` files that you can upload to your ghost site from the `Design` section.
