# Using the theme

To use this theme, you will need to install ghost, for instructions on how to do so see [here](https://ghost.org/docs/install/), or even simpler use [yunohost](https://yunohost.org/) and the [yunohost ghost package](https://github.com/YunoHost-Apps/ghost_ynh).

Then just download the theme as a zip from [it's repository](https://framagit.org/squeak/maghost/), and upload it to your site from the "Design" section in the administrative interface.

To make the best use of maghost, it's deeply recommended that you read the following sections, to understand how to use the theme.


## Homepage images
The homepage background and title images should be selected in the administrative interface under: "Settings>General>Publication cover" and "Settings>General>Publication logo".


## Main pages and languages
The maghost theme is designed for multilingual websites. Since the ghost administrative website doesn't propose multiple titles, pages lists... for different languages you'll have to specify those ones in the "Code injection>Site Header" section.

### Example of "Site Header" section
```js
<script>
  // you must define the "languages" variable, the variable will be analyzed to extract the needed info
  var languages = {
    // the key is important, it's the language code
    en: {
      // text displayed on the homepage under the title picture
      homeSubtitle: "I'm written on the homepage.",
      // the list of pages to display in navigation bar (should be a list of keys from the "pages" object below)
      navbar: [ "home", "pageName", "blog", "contact" ],
      // list of multilingual pages with the paths to their english version and name to display in navigation bar in the english version of the website
      pages: {
        home:            { path: "/",            title: "🏠",               },
        somepage:        { path: "/some-slug/",  title: "My first page",    },
        pageNotInNavBar: { path: "/other-slug/", title: "Other page title", },
        contact:         { path: "/contact/",    title: "Contact me  ",     },
        blog: {
          // to display a blog index page, don't forget to set the /blog/ path to lead to a collection of posts, and to set to use the "blog" template (all this should be done in your routes.yaml file (see routes section for details))
          path: "/blog/",
          title: "Blog", // this will be in navbar but also in page (following the blog template preset)
          intro: "Here is a list of posts I wrote.",
        },
      },
    },
    // another language:
    fr: {
      slug: "/fr/",
      homeSubtitle: "Je suis écrit sur la page d'acceuil.",
      navbar: [ "home", "pageName", "blog", "contact" ],
      pages: {
        home:            { path: "/",               title: "🏠",                  },
        somepage:        { path: "/some-slug-fr/",  title: "Ma première page",    },
        pageNotInNavBar: { path: "/other-slug-fr/", title: "Autre titre de page", },
        contact:         { path: "/contact-fr/",    title: "Me contacter  ",      },
        blog: {
          path: "/blog/",
          title: "Blog",
          intro: "Voici une liste de posts que j'ai écris.",
        },
      },
    },
    // ...
  };
</ script> <!-- if you copy paste this remove the space between "</" and "script>" in this line -->
```

The order of languages is important since languages will be ordered like this list in the language switcher section.
Also, the first language will be considered the default one (if maghostSettings.defaultLanguage is not defined).

### Available options for each language
The full list of available options for each language is the following:
```js
{

  // specifics for home.hbs page
  homeSubtitle: "the subtitle to display under the title image in homepage",

  // the list of pages to display in navigation bar (should be a list of keys from the "pages" object below)
  navbar: [ "the", "list", "of", "pages", "to", "display", "in", "navigation", "bar", "in", "display", "order" ],

  // list of multilingual pages
  pages: {
    [pageName]: {
      !path: <string> « the path to which this page is accessible »
      !title: <string> « the title of this page in navigation bar »
      ?useTitleInPage: <boolean> «
        set this to true, if you want this title to be used in the page itself
        this is useful if the page does not have any written content, like this you can use the same slug for all languages, but still have a title in the appropriate language
        this option is implied in blog, calendar and wiki presets
      »,
      ?intro: <string> « intro used for this language (only used if you set the template of the page to "blog", "calendar" or "wiki") »,
      ?tagsIntro: <string> «
        allows you to introduce the list of language tags that can be shown in the blog page
        if you want this list to work, you need to prefix language tags with "lang:" (e.g. "lang:en", "lang:fr")
        if the tagsIntro option is not defined, the list of language tags will not be shown
      »,
      ?categories: {
        [categoryId <string>]: categoryTitleAndPostIndex <string>,
      } «
        the categories section is only used for wiki indexes
        it allows you to specify some categories for your posts, and the ordering of the articles in this category
        for example, if you want to have a post grouped in a category displayed in the page as "How tos", you could set for example `HOWTO-004` as excerpt of your wiki post, and add `HOWTO: "How tos"` in your categories sections, like this you post will be grouped in the how tos at the fourth place (do not forget to set the appropriate routes in your routes.yaml file, so that sorting is done properly (see routes section for an example))
      »,
    },
    // for the navbar to be able to switch properly between pages in different languages, make sure to use the same pageName (keys) in all languages
  },

}
```


## Additional site options
You can customize some elements of the display using the `maghostSettings` variable.
The variable must be a javascript object, containing pairs of key-value specifying some elements.

For example, you could use something like this:
```js
var maghostSettings = {
  defaultLanguage: "fr",
  navbarPosition: "top",
  navbarSeparators: "·",
  homeSubtitleColor: "#F00",
};
```

The following table lists the available key-value pairs you can use:

| variable name           | value type | default value | possible values                         |
| ----------------------- | ---------- | ------------- | --------------------------------------- |
| defaultLanguage         | string     | "en"          | any key from the languages object       |
| navbarPosition          | string     | "bottom"      | "bottom", "top"                         |
| navbarSeparators        | string     | "/"           | any string                              |
| homeSubtitleColor       | string     | undefined     | any valid css color name/code           |
| numberOfSideImagesToGet | number     | 13            | how many images to load in sides        |
| maghostCustomClasses    | string[]   | []            | any string (for more details see below) |

The `maghostSetting.maghostCustomClasses` variable allows you to apply a custom class to the `maghost` container (the container of all contents in the page). You may pass any class here, that you want to use in your custom stylesheet. But you can also use some premade classes, that allow to easily tweak the display of maghost. The following presets are available:
- `"galleries-small_squares"`: images in galleries will be displayed as square (instead of the responsive width/height)
- `"galleries-bigger_margins"`: will increase the margin between images in galleries


## Customize site colors, content width...
To customize the colors to use in the site, you can add in "Code injection>Site Footer" customization of the styling variables.
For example, to make the content wider, you could add the following:
```html
<style>
  :root {
    --content_width: 60vw;
    --content_margin: 100px;
  }
</style>
```

The list of variables used by the theme, that you can customize is available in [the theme file](styles/general/theme.less).


## Sides images
To create a frieze of images on the sides of pages and posts, create a page with the slug: "/backgrounds/", and add images to it with the "Gallery" utility. Images for the backgrounds will be chosen randomly from the images in this page.
For this to work, you should also add the backgrounds page as custom route as follow (see next section for more details on that):
```yaml
routes:
  /backgrounds/:
    template: meta
    data: page.backgrounds
```


## Site's routes

The following is the recommended content to put in your `routes.yaml` file.

> You can edit the routes file from the administrative interface under: "Labs>Beta Features>Routes"

```yaml
# list of custom routes
routes:
  # homepage should use home.hbs template
  /: home
  # backgrounds page should use custom meta.hbs template, providing backgrounds page data
  # this also means that for backgrounds to work, you must create a page with slug /backgrounds/ and add some images in it (see previous section for details)
  /backgrounds/:
    template: meta
    data: page.backgrounds

# list of collections (a post can only be in one collection at a time)
collections:
  # blog posts
  /blog/:
    permalink: /blog/{slug}/
    template: blog
    filter: 'tag:-event' # only blog posts without the event tag
  # calendar events (identified by the "#event" tag)
  /calendar/:
    permalink: /calendar/{slug}/
    template: calendar
    filter: 'tag:hash-event' # only blog posts with the #event tag
    order: 'excerpt desc' # here the idea is to use excerpt to provide the event date (and location), this makes sure that posts are ordered by date
  # wiki articles (identified by the "#wiki" tag)
  /wiki/:
    permalink: /wiki/{slug}/
    template: wiki
    filter: 'tag:hash-wiki' # only blog posts with the #wiki tag
    order: 'excerpt asc' # here the idea is to use excerpt to sort wiki articles (and event to group them by categories, see details in the "pages templates" section)

# taxonomies: allowing to have page listing posts by author or by tag
taxonomies:
  tag: /tag/{slug}/
  author: /author/{slug}/
```


## Pages templates

Maghost let's you display nicely collections of blog posts, events and wiki articles.
To use one of those styling of collections, you will need to setup your routes.yaml file with something similar than what's described in the previous section.

The `blog` template displays a page listing matching blog posts, with previews or excerpt, authors and dates...

The `calendar` template displays matching posts with a bit more focus on their content, so that user does not necessarily have to open the event to read the content, also it doesn't specify the authors of the posts, but showing the excerpt like a date/place would be shown, so you're greatly encouraged to use excerpt in events to display those elements, in the format `YYYY-MM-DD · Place`, for example, to allow a good sorting of your events.

The `wiki` template displays the posts in a very dense way, only showing their title. It also organizes them by categories. For more details on how to create categories for your wiki posts, check the `options for each language` section.

One additional thing to note is that if you use the hidden tags "#event" and "#wiki" for your posts, the post page will avoid showing the "author" header and "next and previous posts" footer.
