# Social and local

## Social bugs or features?

Maybe you noticed that filling the social networks fields of authors, posts, just does nothing, they're not displayed anywhere. Maybe this looks like a bug, but it is intended as a feature, an opportunity to forget for a while of facebook, twitter and co.
You're welcome to modify maghost and create your own version of it featuring links to social networks, but please don't ask me to add them, I removed them for a reason :)

## Localization

In the usage page, you may have noted that you can create a multilingual site. The core interface of maghost is translated in a few languages (english, french and italian at the moment). If you want to create a site in another language, you should probably add the corresponding locale in the `/locale/` directory of the theme (then you should run `npm run buildDists` for your changes to take effect (see the next section "Developing")).
You're very welcome to send the locale you created to add it to this repository, so that other people can benefit from it :)
