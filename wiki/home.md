
Maghost is a theme for [the ghost blogging platform](https://ghost.org/). It allows you to create easily a website and blog that you can simply edit with the ghost administrative interface.

This theme goal is to provide a simple multilingual website. Since ghost doesn't make it especially easy to have multilingual contents, and to make a nice website (on top of a blog), there are few tips and tricks that you'll need to follow to get the best out of maghost.
