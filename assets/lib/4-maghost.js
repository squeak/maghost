
const queryParams = new URLSearchParams(window.location.search);

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

const maghost = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  QUERY PARAMETERS GETTER/SETTER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get value from current url query
    ARGUMENTS: ( !key <string> « key you want to get value of » )
    RETURN: <any>
  */
  queryGet: function (key) { return queryParams.get(key); },

  /**
    DESCRIPTION: set value from current url query
    ARGUMENTS: (
      !key <string> « key you want to get value of »,
      !value <any> « value to set »,
    )
    RETURN: <void>
  */
  querySet: function (key, value) {
    queryParams.set(key, value);
    let newurl = window.location.protocol +"//"+ window.location.host + window.location.pathname +"?"+ queryParams.toString();
    // window.history.pushState({ path: newurl }, "", newurl); // current state will be kept in history (previous button will go back to it)
    window.history.replaceState({ path: newurl }, "", newurl); // current state will not be kept in history (previous button will not go back to it)
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET LOCALE FROM BROWSER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: check browser preferences and return locale code matching the one of the browser (if browser locale is a supported language)
    ARGUMENTS: ( ø )
    RETURN: <string|undefined>
  */
  getLocaleFromBrowser: function () {

    let browserLanguage = window.navigator.userLanguage || window.navigator.language;

    // iterate supportedLanguages to see if browser language is supported
    let i = 0;
    let supportedLanguageCodes = _.keys(languages);
    while (i < supportedLanguageCodes.length) {
      if (browserLanguage.match(
        new RegExp("^"+ supportedLanguageCodes[i])
      )) return supportedLanguageCodes[i];
      i++;
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LOCALIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: replace all strings in page that should be translated, with the appropriate string in the current locale
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  localize: function () {
    $("[maglocalize]").each(function () {
      $(this).text(locale.strings[$(this).attr("maglocalize")]);
    })
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET LOCALE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: set the current locale, page width (set it to query, localstorage, html, setup locale strings)
    ARGUMENTS: ( newLocaleCode <string> « the new locale code to use » )
    RETURN: <void>
  */
  setLocale: function (newLocaleCode) {

    // SET GENERAL localeCode AND locale FROM NEW LOCALE CODE
    localeCode = newLocaleCode;
    locale = languages[newLocaleCode] || {};
    if (!locale.pages) locale.pages = {}; // make sure that locale.pages exist, to avoid throwing errors

    // SET LOCALE STRINGS FOR CURRENT LOCALE
    locale.strings = localeStrings && localeStrings[newLocaleCode] ? localeStrings[newLocaleCode] : {};

    // SAVE localeCode TO localStorage, QUERY AND HEAD
    maghost.querySet("l", newLocaleCode);
    localStorage.setItem("maghostLocaleCode", newLocaleCode);
    $("html").attr("lang", newLocaleCode);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET CONTRAST COLOR FOR THE GIVEN COLO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get contrast color (white or black) for the given color
    ARGUMENTS: ( !color <hexString> )
    RETURN: <number>
  */
  contrastColor: function (color) {

    // CONVERT HEX COLOR TO RGB (http://gist.github.com/983661)
    color = +("0x" + color.slice(1).replace(color.length < 5 && /./g, '$&$&'));

    var red = color >> 16;
    var green = color >> 8 & 255;
    var blue = color & 255;

    // RETURN HSP (CALCULATION INSPIRED BY: http://alienryderflex.com/hsp.html)
    if (Math.sqrt(
      0.299 * (red * red) +
      0.587 * (green * green) +
      0.114 * (blue * blue)
    ) < 127.5) return "white"
    else return "black";

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  STICKY NAV TITLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: used on invividual post pages, displays the post title in place of the nav bar when scrolling past the title
    ARGUMENTS: ({
      !navSelector: <string> « query selector to get the nav bar »,
      !titleSelector: <string> « query selector to get the title »,
      !activeClass: <string> « class to add to the nav bar when title should be shown in it »,
    })
    RETURN: <void>
    EXAMPLES:
      maghost.stickyTitle({
        navSelector: ".site-nav-main",
        titleSelector: ".post-full-title",
        activeClass: "nav-post-title-active",
      });
  */
  stickyNavTitle: function (options) {
    var nav = document.querySelector(options.navSelector);
    var title = document.querySelector(options.titleSelector);

    var lastScrollY = window.scrollY;
    var ticking = false;

    function onScroll () {
      lastScrollY = window.scrollY;
      requestTick();
    }

    function requestTick () {
      if (!ticking) requestAnimationFrame(update);
      ticking = true;
    };

    function update () {
      var trigger = title.getBoundingClientRect().top + window.scrollY;
      var triggerOffset = title.offsetHeight + 35;

      // show/hide post title
      if (lastScrollY >= trigger + triggerOffset) nav.classList.add(options.activeClass)
      else nav.classList.remove(options.activeClass);

      ticking = false;
    };

    window.addEventListener("scroll", onScroll, { passive: true });

    update();
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INFINITE SCROLL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: list of functions to execute when infinite scroll has started loading new additional posts
    TYPE: <<function(ø):void>[]>
  */
  infiniteScrollSubscriptions: [],

  /**
    DESCRIPTION:
      Used on all pages where there is a list of posts (blog page, tag index, etc).

      When the page is scrolled to 300px from the bottom, the next page of posts
      is fetched by following the the <link rel="next" href="..."> that is output
      by {{ghost_head}}.

      The individual post items are extracted from the fetched pages by looking for
      a wrapper element with the class "maghost-feed-card". Any found elements are
      appended to the element with the class "maghost-feed" in the currently viewed page.
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  infiniteScroll: function () {
    // next link element (get the link to the second page of posts)
    var nextElement = document.querySelector("link[rel=next]");
    if (!nextElement) return;

    // post feed element
    var feedElement = document.querySelector(".maghost-feed");
    if (!feedElement) return;

    var buffer = 300;

    var ticking = false;
    var loading = false;

    var lastScrollY = window.scrollY;
    var lastWindowHeight = window.innerHeight;
    var lastDocumentHeight = document.documentElement.scrollHeight;

    function onPageLoad () {
      if (this.status === 404) {
        window.removeEventListener('scroll', onScroll);
        window.removeEventListener('resize', onResize);
        return;
      };

      // append contents
      var postElements = this.response.querySelectorAll('.maghost-feed-card');
      postElements.forEach(function (item) {
        // document.importNode is important, without it the item's owner
        // document will be different which can break resizing of
        // `object-fit: cover` images in Safari
        feedElement.appendChild(document.importNode(item, true));
      });

      // set next link
      var resNextElement = this.response.querySelector('link[rel=next]');
      if (resNextElement) nextElement.href = resNextElement.href
      else {
        window.removeEventListener('scroll', onScroll);
        window.removeEventListener('resize', onResize);
      };

      // sync status
      lastDocumentHeight = document.documentElement.scrollHeight;
      ticking = false;
      loading = false;

      // activate subsriptions
      _.each(maghost.infiniteScrollSubscriptions, function (subscriptionMethod) { subscriptionMethod(); });

    };

    function onUpdate () {
      // return if already loading
      if (loading) return;

      // return if not scroll to the bottom
      if (lastScrollY + lastWindowHeight <= lastDocumentHeight - buffer) { ticking = false; return; }

      loading = true;

      var xhr = new window.XMLHttpRequest();
      xhr.responseType = 'document';

      xhr.addEventListener('load', onPageLoad);

      xhr.open('GET', nextElement.href);
      xhr.send(null);
    };

    function requestTick () {
      ticking || window.requestAnimationFrame(onUpdate);
      ticking = true;
    };
    function onScroll () {
      lastScrollY = window.scrollY;
      requestTick();
    };
    function onResize () {
      lastWindowHeight = window.innerHeight;
      lastDocumentHeight = document.documentElement.scrollHeight;
      requestTick();
    };

    window.addEventListener('scroll', onScroll, {passive: true});
    window.addEventListener('resize', onResize);

    requestTick();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  RESIZE IMAGES IN GALLERIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: gallery card support: detects when a gallery card has been used and applies sizing to make sure the display matches what is seen in the editor
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  resizeImagesInGalleries: function () {
    var images = document.querySelectorAll(".kg-gallery-image img");
    images.forEach(function (image) {
      var container = image.closest(".kg-gallery-image");
      var width = image.attributes.width.value;
      var height = image.attributes.height.value;
      var ratio = width / height;
      container.style.flex = ratio + ' 1 0%';
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE IMAGES IN GALLERIES CLICKABLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make all images in galleries clickable to display fullscreen
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  makeImageGalleriesClickable: function () {

    // add src as href of each image
    $(".kg-gallery-image img").each(function () { $(this).attr("href", $(this).attr("src")); });

    // colorboxify images (= make them clickable to display in big)
    $(".kg-gallery-image img").colorbox({
      rel: "imagesGroup",
      transition:"none",
      // opacity: 0.95,
      maxWidth: "97%",
      maxHeight:"97%",
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
