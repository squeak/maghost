#!/bin/bash

# zip
# -r: recursive
# target zip file
# content to zip
# -x: exclude files/directories
zip -r maghost.zip * -x "node_modules/*" "package-lock.json" ".git/*"
