
//
//                              GLOBAL VARIABLES

// make sure necessary variables that site administrator should pass are defined
if (typeof maghostSettings === "undefined") var maghostSettings = {};
if (typeof languages === "undefined" || !languages) var languages = {};

//
//                              DETERMINE localeCode FROM:

// 1. query
let localeCode = maghost.queryGet("l");

// 2. localStorage
if (!localeCode) localeCode = localStorage.getItem("maghostLocaleCode");

// 3. browser prefences
if (!localeCode) localeCode = maghost.getLocaleFromBrowser();

// 4. maghostSettings.defaultLanguage
if (!localeCode) localeCode = maghostSettings.defaultLanguage || _.keys(languages)[0] || "en";

//
//                              SET LOCALE

let locale;
maghost.setLocale(localeCode);

//
//                              FIGURE OUT IF CURRENT PAGE IS IN LIST OF MULTILINGUAL PAGES

// get page key from query
let askedPageKey = maghost.queryGet("p");

// if page was not specified in query, try to figure it out from slug
if (!askedPageKey) {
  let currentPageSlug = window.location.pathname.replace(/^\//, "").replace(/\/$/, "");
  // if current page slug is slug of a page (in any language), set "askedPageKey" and "p"
  for (localeKey in languages) {

    let shouldBreak = false;

    // iterate pages for this language, to see if one of them has same slug as current page
    for (pageKey in languages[localeKey].pages) {
      if ((languages[localeKey].pages[pageKey].path || "").replace(/^\//, "").replace(/\/$/, "") === currentPageSlug) {
        // set asked page
        askedPageKey = pageKey;
        maghost.querySet("p", askedPageKey);
        // if current locale is different than the one of the page, reset locale
        if (localeCode !== localeKey) maghost.setLocale(localeKey);
        // break both iterations
        shouldBreak = true;
        break;
      };
    };

    if (shouldBreak) break;

  };
};

//                              ¬
//
