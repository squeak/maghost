
$(document).ready(function () {

  // hover on avatar
  var hoverTimeout;
  $(".maghost-hoverable-avatar").hover(function () {
    var $this = $(this);

    clearTimeout(hoverTimeout);

    $(".maghost-author_card").removeClass("hovered");
    $(this).children(".maghost-author_card").addClass("hovered");

  }, function () {
    var $this = $(this);

    hoverTimeout = setTimeout(function () {
      $this.children(".maghost-author_card").removeClass("hovered");
    }, 800);

  });
});
