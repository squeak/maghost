
// REPLACE TITLE OF PAGE/POST BY THE ONE IN NAVBAR, IF SPECIFIED IN LOCALE OPTIONS
$(document).ready(function () {

  if (
    locale &&
    locale.pages &&
    locale.pages[askedPageKey] &&
    // current asked page should use navbar title
    locale.pages[askedPageKey].useTitleInPage &&
    // there is a navbar title
    locale.pages[askedPageKey].title
  ) $(".maghost-page_post-header-title").html(locale.pages[askedPageKey].title);

});
