
// REMOVE "READ MORE" IF THERE IS NO OVERFLOWING TEXT
$(function () {
  $(".maghost-event_card-content").each(function () {
    if (!($(this)[0].clientHeight < $(this)[0].scrollHeight)) $(this).next().hide();
  });
});
