#!/bin/bash

# MERGE AND CONVERT LESS STYLES TO CSS
find ./styles -type f -name "*.less" -exec cat {} \; > assets/built/styles.less
./node_modules/.bin/lessc assets/built/styles.less assets/built/styles.css
# cat partials-styles/*.css > assets/built/partials.css

# MERGE ALL JS LIBRARIES
cat ./assets/lib/*.js > assets/built/libraries.js
# find ./assets/lib -type f -name "*.js" -exec cat {} \; > assets/built/libraries.js

# MERGE ALL LOCALES
cat ./locale/*.js > assets/built/locales.js

# MERGE ALL SCRIPTS
find ./scripts -type f -name "*.js" -exec cat {} \; > assets/built/scripts.js
# cat partials-scripts/*.js > assets/built/partials.js

# ECHO RESULT
RED='\033[0;31m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color
if [[ "$?" != "0" ]]; then
  echo -e "${RED}Error building styles and scripts!${NC}"
else
  echo -e "${GREEN}Successfully built styles and scripts!${NC}"
fi
