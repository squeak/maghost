# maghost


Maghost is a theme for [the ghost blogging platform](https://ghost.org/). It allows you to create easily a multilingual website and blog that you can simply edit with the ghost administrative interface.

![screenshot](./assets/screenshot.png)

For the full documentation, usage instructions... check [maghost documentation page](https://squeak.eauchat.org/maghost/).


## Copyright & License

This theme is licensed under GNU-AGPLv3 license.
It is inspired from the ghost [casper theme](https://github.com/TryGhost/Casper) (Copyright (c) 2013-2020 Ghost Foundation - Released under the MIT license). This theme is radically different from casper, however some contents from it are used in this theme.
